﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Fisher;
using System.Collections.Generic;

namespace Fishtyping
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        //player + bucket + boat
        Player myPlayer;

        //Hook
        Hook myHook;

        //Sea background
        Texture2D seaBackground;
        Rectangle seaRect;
        float seaScale;
        Rectangle seaArea;


        //Parallaxing sky
        ParallaxingBackground skyBackground;

        //govern how fast our fish appear
        Texture2D[] fishTexture;
        TimeSpan fishSpawnTime;
        TimeSpan previousFishSpawnTime;
        List<Fish> fishCollection;
        int numberOfFish;

        //score
        Score myScore;

        //sound
        SoundEffect bgSound;
        SoundEffectInstance bgSoundInstance;

        //test
        //Texture2D wallTexture;
        //Rectangle wallRect;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            //graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            skyBackground = new ParallaxingBackground();

            myPlayer = new Player();

            myHook = new Hook();

            myScore = new Score();

            //fish
            fishTexture = new Texture2D[4];
            fishCollection = new List<Fish>();
            const float SECONDS_IN_MINUTE = 60f;
            const float RATE_OF_FISH = 30f;
            fishSpawnTime = TimeSpan.FromSeconds(SECONDS_IN_MINUTE / RATE_OF_FISH);

            //test
            //wallRect = new Rectangle(300, 0, 1, 1000);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            skyBackground.Initialize(Content.Load<Texture2D>("images/sky"), GraphicsDevice.Viewport, 69);

            seaBackground = Content.Load<Texture2D>("images/water");
            seaScale = Math.Max((float)graphics.PreferredBackBufferWidth / seaBackground.Width, (float)(graphics.PreferredBackBufferHeight - skyBackground.bgHeight) / seaBackground.Height);
            seaRect = new Rectangle(0, skyBackground.bgHeight, (int)(seaBackground.Width * seaScale), (int)(seaBackground.Height * seaScale));
            seaArea = new Rectangle(0, skyBackground.bgHeight, graphics.PreferredBackBufferWidth - seaRect.X, graphics.PreferredBackBufferHeight - seaRect.Y);


            myPlayer.LoadContent(Content.Load<Texture2D>("images/boat")
                , Content.Load<Texture2D>("images/baby")
                , Content.Load<Texture2D>("images/bucket")
                , Content.Load<SoundEffect>("Sounds/CautghtFish")
                , Content.Load<SoundEffect>("Sounds/Shocked"));
            myPlayer.Initialize(seaArea);

            //Fish texture
            fishTexture[0] = Content.Load<Texture2D>("images/fish1");
            fishTexture[1] = Content.Load<Texture2D>("images/fish2");
            fishTexture[2] = Content.Load<Texture2D>("images/fish3");
            fishTexture[3] = Content.Load<Texture2D>("images/eel");
            
            //score
            myScore.Initialize(Content.Load<Texture2D>("images/score")
                , new Vector2(GraphicsDevice.Viewport.Width / 3, 10)
                , Content.Load<Texture2D>("images/warn")
                , Content.Load<Texture2D>("images/time")
                , new Vector2(GraphicsDevice.Viewport.Width / 3 * 2, 10)
                , new Vector2(5, 5)
                , Content.Load<SpriteFont>("Verdana15")
                , Content.Load<SpriteFont>("Verdana28")
                , Content.Load<SoundEffect>("Sounds/GameWin")
                , Content.Load<SoundEffect>("Sounds/GameLose"));

            //Hook
            myHook.Initialize(Content.Load<Texture2D>("images/hook")
                , new Vector2(myPlayer.boatPosition.X + 40, myPlayer.boatPosition.Y + 5)
                , myScore.myLevel * 150f + 150
                , Content.Load<Texture2D>("images/string")
                , GraphicsDevice.Viewport
                , Content.Load<SoundEffect>("Sounds/HookDown"));

            numberOfFish = myScore.myLevel * 5;

            //test
            //wallTexture = Content.Load<Texture2D>("images/string");

            //BGsound
            bgSound = Content.Load<SoundEffect>("Sounds/MadagascarBGMusic");
            bgSoundInstance = bgSound.CreateInstance();
            bgSoundInstance.IsLooped = true;
            bgSoundInstance.Volume -= 0.5f;
            bgSoundInstance.Play();

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            myScore.Unload();
            myPlayer.Unload();
            myHook.Unload();
            bgSoundInstance.Dispose();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            skyBackground.Update(gameTime);

            if (myHook.hookRunning)
            {
                myHook.Update(gameTime);
            }
            else
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    myHook.hookRunning = true;
                    myHook.PlayHookSound();
                }
                else
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.Left))
                    {
                        myPlayer.boatPosition.X -= 40f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        
                    }
                    else if (Keyboard.GetState().IsKeyDown(Keys.Right))
                    {
                        myPlayer.boatPosition.X += 40f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }
                }
                myHook.hookRectangle.X = (int)myPlayer.boatPosition.X + 40;
                myHook.hookRectangle.Y = (int)myPlayer.boatPosition.Y + 5;
            }

            myPlayer.Update(gameTime);


            UpdateCollisions();

            myScore.Update(gameTime);

            UpdateScore();

            AddFish(gameTime);
            fishCollection.ForEach(fish => fish.Update(gameTime));

            base.Update(gameTime);
        }

       
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            skyBackground.Draw(spriteBatch);

            myPlayer.Draw(spriteBatch);

            spriteBatch.Draw(seaBackground, seaRect, Color.White);

            fishCollection.ForEach(fish => fish.Draw(spriteBatch));

            myHook.Draw(spriteBatch);

            myScore.Draw(spriteBatch, gameTime);
            //spriteBatch.Draw(wallTexture, wallRect, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void AddFish(GameTime gameTime)
        {
            if (gameTime.TotalGameTime - previousFishSpawnTime > fishSpawnTime && fishCollection.Count < numberOfFish)
            {
                previousFishSpawnTime = gameTime.TotalGameTime;

                //AddFish
                Fish fish = new Fish();

                Random rnd = new Random();


                //X: left or right, Y: from above level to end viewport - max(height of all fishes)
                Vector2 position = new Vector2(seaRect.Width * rnd.Next(0, 2)
                    , rnd.Next(seaRect.Y + 80, seaRect.Y + seaRect.Height - 80));

                fish.Initialize(fishTexture, rnd.Next(0, 100) % 4 + 1, position);

                fishCollection.Add(fish);
            }
        }
        
        private void UpdateCollisions()
        {
            for (int i = fishCollection.Count - 1; i >= 0; i--)
            {
                Rectangle fishRectangle = fishCollection[i].GetRectangle();

                if (myHook.hookSpeed > 0 && !fishCollection[i].beingCaught && fishRectangle.Intersects(myHook.hookRectangle))
                {
                    //roll back the hook
                    myHook.CaughtFish();

                    //fish is being caught
                    if (!fishCollection[i].BeingCaught(myHook.hookSpeed, myHook.hookRectangle))
                    {
                        myPlayer.GetElectricShock();
                        myScore.SubstracScore();
                        return;
                    }

                    //record score   
                    myScore.pendingScore = true;
                }

                if (!seaArea.Intersects(fishCollection[i].GetRectangle()))
                {
                    fishCollection.RemoveAt(i);
                }
            }
        }

        private void UpdateScore()
        {
            //Roll the hook;
            if (myScore.gameStatus != -1)
            {
                myHook.CaughtFish();
                //win the game
                if (myScore.gameStatus == 1)
                {
                    myPlayer.PlayerWin();
                }
                //lose the game
                else
                {
                    myPlayer.PlayerLose();
                }

                myScore.gameStatus = -1;
                numberOfFish = myScore.myLevel * 5;
                myHook.hookSpeed = myScore.myLevel * 50f + 250;
            }
            else
            {
                if (myScore.pendingScore && myHook.StringLength() == 0)
                {
                    myScore.pendingScore = false;
                    myScore.AddScore();
                    myPlayer.CaughtAFish();
                }
            }
        }
    }
}
