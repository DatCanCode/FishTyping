﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Fishtyping
{
    class Score
    {
        Texture2D scoreTexture;
        Vector2 scorePosition;

        Texture2D timeTexture;
        Vector2 timePosition;

        Animation warnAnimation;

        SpriteFont infoTextTexture;
        Vector2 scoreTextPosition;
        Vector2 timeTextPosition;

        Color timeColor;

        int myScore;

        int levelTarget;

        TimeSpan elapsedTime;

        public int myTime { get; private set; }

        public int myLevel { get; private set; }

        public bool pendingScore { get; set; }

        public int gameStatus { get; set; }

        string gameResult;
        int gameResultDuration;
        Vector2 gameResultPosition;

        SpriteFont bigTextTexture;

        //Sound
        private SoundEffect winSound;
        private SoundEffectInstance winSoundInstance;

        private SoundEffect loseSound;
        private SoundEffectInstance loseSoundInstance;

        public void Initialize(Texture2D _scoreTexture, Vector2 _scorePosition, Texture2D _warnTexture, Texture2D _timeTexture, Vector2 _timePosition, Vector2 _gameResultPosition, SpriteFont _textTexture, SpriteFont _bigTextTexture, SoundEffect _winSound, SoundEffect _loseSound)
        {
            infoTextTexture = _textTexture;
            bigTextTexture = _bigTextTexture;

            scoreTexture = _scoreTexture;

            scorePosition = _scorePosition;
            scoreTextPosition = new Vector2(scorePosition.X + 85, scorePosition.Y + 21);

            warnAnimation = new Animation();
            warnAnimation.LoadContent(_warnTexture);
            warnAnimation.Initialize(new Vector2(_scorePosition.X + 64, _scorePosition.Y + 36)
                , 3
                , 100
                , 151
                , 89
                , 11
                , 1f
                , false);
            warnAnimation.active = false;

            timeTexture = _timeTexture;
            timePosition = _timePosition;

            timeTextPosition = new Vector2(_timePosition.X + 5, _timePosition.Y + 20);

            myLevel = 1;
            
            timeColor = Color.DarkBlue;

            gameResultPosition = _gameResultPosition;

            winSound = _winSound;
            winSoundInstance = winSound.CreateInstance();

            loseSound = _loseSound;
            loseSoundInstance = loseSound.CreateInstance();

            GameRoundInfo();
        }

        public void Unload()
        {
            winSoundInstance.Dispose();
            loseSoundInstance.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            warnAnimation.Update(gameTime);

            if (gameTime.TotalGameTime - elapsedTime >= TimeSpan.FromSeconds(1))
            {
                elapsedTime = gameTime.TotalGameTime;
                myTime--;

                if (gameResultDuration > 0)
                {
                    gameResultDuration--;
                }
            }

            if (myTime <= 7 && timeColor != Color.Red)
            {
                timeColor = Color.Red;
            }

            if (myTime <= 0)
            {
                timeColor = Color.DarkBlue;

                //Win
                if (myScore >= levelTarget)
                {
                    myLevel++;
                    gameStatus = 1;
                    gameResult = "Level UP!";
                    gameResultDuration = 2;
                    winSoundInstance.Play();
                }
                else
                {
                    myLevel = 1;
                    gameStatus = 0;
                    gameResult = "You Lose!";
                    gameResultDuration = 2;
                    loseSoundInstance.Play();
                }


                GameRoundInfo(true);
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            warnAnimation.Draw(spriteBatch);

            spriteBatch.Draw(scoreTexture, scorePosition, Color.White);
            spriteBatch.DrawString(infoTextTexture, myScore.ToString(), scoreTextPosition, Color.Black);
            
            spriteBatch.Draw(timeTexture, timePosition, Color.AliceBlue);
            spriteBatch.DrawString(infoTextTexture, String.Format("{0:#}\\{1:##}", myLevel, myTime), timeTextPosition, timeColor);

            if (gameResultDuration > 0)
            {
                spriteBatch.DrawString(bigTextTexture, gameResult, gameResultPosition, Color.Red);
            }
        }

        public void AddScore()
        {
            myScore++;
        }

        public void SubstracScore()
        {
            if (myScore > 0)
            {
                myScore--;
            }
            warnAnimation.active = true;
            warnAnimation.enableAnimation = true;
        }

        public void GameRoundInfo(bool newLevel = false)
        {
            myTime = 55 + myLevel * 5;

            levelTarget = myLevel * 5 + 5;

            myScore = 0;

            gameResultDuration = 5;
            if (!newLevel)
            {
                gameResult = "";
            }
            gameResult += "\nRound " + myLevel + ": " + levelTarget;

            gameStatus = -1;
        }
    }
}
