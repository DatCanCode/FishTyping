﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Fishtyping;

namespace Fisher
{
    class Player
    {
        public Texture2D boatTexture;
        public Vector2 boatPosition;

        public float boatSpeed;

        Animation playerAnimation = new Animation();

        Animation shockedPlayerAnimation = new Animation();

        Animation winPlayerAnimation = new Animation();

        Animation losePlayerAnimation = new Animation();

        Animation bucketAnimation = new Animation();

        Rectangle seaArea;

        Vector2 sinkLevel;

        //sound
        SoundEffect caughtFishSound;
        SoundEffectInstance caughtFishSoundInstance;

        SoundEffect getShockedSound;
        SoundEffectInstance getShockedSoundInstance;


        public void LoadContent(Texture2D _boatTexture, Texture2D _playerTexture, Texture2D _bucketTexture, SoundEffect _caughtFishSound, SoundEffect _getShockedSound)
        {
            boatTexture = _boatTexture;

            playerAnimation.LoadContent(_playerTexture);
            shockedPlayerAnimation.LoadContent(_playerTexture);
            winPlayerAnimation.LoadContent(_playerTexture);
            losePlayerAnimation.LoadContent(_playerTexture);

            bucketAnimation.LoadContent(_bucketTexture);

            caughtFishSound = _caughtFishSound;
            caughtFishSoundInstance = caughtFishSound.CreateInstance();

            getShockedSound = _getShockedSound;
            getShockedSoundInstance = getShockedSound.CreateInstance();
        }

        public void Initialize(Rectangle _seaArea)
        {
            seaArea = _seaArea;

            sinkLevel = new Vector2(7, 14);

            boatSpeed = 3f;

            //Boat
            this.boatPosition = new Vector2(seaArea.Width / 2 - boatTexture.Width / 2, seaArea.Y - boatTexture.Height + sinkLevel.X);

            //Player
            playerAnimation.Initialize(new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3)
                , 16
                , 100
                , 127
                , 170
                , 32
                , 0.7f
                , true);

            //ShockedPlayer
            shockedPlayerAnimation.Initialize(new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3)
                , 16
                , 80
                , 127
                , 170
                , 16
                , 0.7f
                , false
                , false
                , 85);
            shockedPlayerAnimation.active = false;

            //winPlayerAnimation
            winPlayerAnimation.Initialize(new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3)
                , 16
                , 100
                , 127
                , 170
                , 39
                , 0.7f
                , false
                , false
                , 32);
            winPlayerAnimation.active = false;

            //losePlayerAnimation
            losePlayerAnimation.Initialize(new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3)
                , 16
                , 80
                , 127
                , 170
                , 14
                , 0.7f
                , false
                , false
                , 71);
            losePlayerAnimation.active = false;

            //Bucket
            bucketAnimation.Initialize(new Vector2(boatPosition.X + 65, boatPosition.Y + 37)
                , 4
                , 70
                , 231
                , 141
                , 11
                , 0.65f
                , false
                , false);
        }


        public void Unload()
        {
            caughtFishSoundInstance.Dispose();
            getShockedSoundInstance.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            ///Console.WriteLine((boatPosition.Y + boatTexture.Height) - seaArea.Y);
            if ((boatPosition.Y + boatTexture.Height) - seaArea.Y > sinkLevel.Y || (boatPosition.Y + boatTexture.Height) - seaArea.Y < sinkLevel.X)
            {
                boatSpeed *= -1;
            }
            boatPosition.Y += boatSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;


            if (shockedPlayerAnimation.active)
            {
                shockedPlayerAnimation.position = new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3);
                shockedPlayerAnimation.Update(gameTime);
                
                if (!shockedPlayerAnimation.enableAnimation)
                {
                    shockedPlayerAnimation.active = false;
                    playerAnimation.active = true;
                }
            }
            else if (winPlayerAnimation.active)
            {
                winPlayerAnimation.position = new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3);
                winPlayerAnimation.Update(gameTime);

                if (!winPlayerAnimation.enableAnimation)
                {
                    winPlayerAnimation.active = false;
                    playerAnimation.active = true;
                }
            }
            else if (losePlayerAnimation.active)
            {
                losePlayerAnimation.position = new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3);
                losePlayerAnimation.Update(gameTime);

                if (!losePlayerAnimation.enableAnimation)
                {
                    losePlayerAnimation.active = false;
                    playerAnimation.active = true;
                }
            }
            else
            {
                playerAnimation.position = new Vector2(boatPosition.X + boatTexture.Width / 2, boatPosition.Y + boatTexture.Height / 3);
                playerAnimation.Update(gameTime);
            }

            bucketAnimation.position = new Vector2(boatPosition.X + 65, boatPosition.Y + 37);
            bucketAnimation.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            playerAnimation.Draw(spriteBatch);
            shockedPlayerAnimation.Draw(spriteBatch);
            winPlayerAnimation.Draw(spriteBatch);
            losePlayerAnimation.Draw(spriteBatch);

            bucketAnimation.Draw(spriteBatch);
            spriteBatch.Draw(boatTexture, boatPosition, Color.White);

        }

        public void CaughtAFish()
        {
            bucketAnimation.enableAnimation = true;
            caughtFishSoundInstance.Play();
        }

        public void GetElectricShock()
        {
            playerAnimation.active = false;
            shockedPlayerAnimation.active = true;
            shockedPlayerAnimation.enableAnimation = true;
            getShockedSoundInstance.Play();
        }

        public void PlayerWin()
        {
            playerAnimation.active = false;
            winPlayerAnimation.active = true;
            winPlayerAnimation.enableAnimation = true;
        }

        public void PlayerLose()
        {
            playerAnimation.active = false;
            losePlayerAnimation.active = true;
            losePlayerAnimation.enableAnimation = true;
        }
    }
}
